library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture behavior of cache is

    SUBTYPE cache_entry_t IS std_logic_vector( (7-1) downto 0 );
    TYPE cache_struct IS array (0 to 27-1) of cache_entry_t;
--27 Eintraege
    CONSTANT cache : cache_struct := (
    "1011110",
    "1001010",
    "1111110",
    "1101111",
    "1111111",
    "1011101",
    "1001010",
    "1111000",
    "1111110",
    "1011011",
    "0101101",
    "1001000",
    "1001100",
    "1111111",
    "1001011",
    "1111101",
    "1001010",
    "1101111",
    "1011011",
    "1101101",
    "1001010",
    "1101000",
    "1111010",
    "1010011",
    "1011001",
    "1101100",
    "1010000"
    );

begin

PROCESS (clk)
    
    VARIABLE tag : std_logic_vector( 3-1 downto 0);
    VARIABLE index: integer range 0 to 256;

    VARIABLE data_tmp : std_logic_vector( 4-1 downto 0);
    VARIABLE ch_cm_tmp : std_logic;

    BEGIN

        data_tmp := (OTHERS => 'Z');
        ch_cm_tmp := '0';

        IF( falling_edge( clk) ) THEN
        
            IF (en_read = '1') THEN
--CHECKEN fuer Pruefung !!!
                index := to_integer(unsigned(addr(4 downto 0)));
                tag := addr(8-1 downto 5);

                IF (index <= cache'high) AND (cache(index)(7-1 downto 4) = tag) THEN
    
                    data_tmp := cache(index)(4-1 downto 0);
                    ch_cm_tmp := '1';

                END IF;
--END CHECKEN
            END IF;

            data <= data_tmp;
            ch_cm <= ch_cm_tmp;

        END IF;

END PROCESS;

end behavior;

