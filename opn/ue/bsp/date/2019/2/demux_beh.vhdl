library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture behavior of demux is

begin

PROCESS(IN1, SEL) 

--VARIABLES, SIGNALS come here

BEGIN

    IF (SEL = "000") THEN
        OUT1 <= IN1;
        OUT2 <= (others => '0');
        OUT3 <= (others => '0');
        OUT4 <= (others => '0');
        OUT5 <= (others => '0');
        OUT6 <= (others => '0');
    ELSIF (SEL = "001") THEN
        OUT2 <= IN1;
        OUT1 <= (others => '0');
        OUT3 <= (others => '0');
        OUT4 <= (others => '0');
        OUT5 <= (others => '0');
        OUT6 <= (others => '0');
    ELSIF (SEL = "010") THEN
        OUT3 <= IN1;
        OUT2 <= (others => '0');
        OUT1 <= (others => '0');
        OUT4 <= (others => '0');
        OUT5 <= (others => '0');
        OUT6 <= (others => '0');
    ELSIF (SEL = "011") THEN
        OUT4 <= IN1;
        OUT2 <= (others => '0');
        OUT3 <= (others => '0');
        OUT1 <= (others => '0');
        OUT5 <= (others => '0');
        OUT6 <= (others => '0');
    ELSIF (SEL = "100") THEN
        OUT5 <= IN1;
        OUT2 <= (others => '0');
        OUT3 <= (others => '0');
        OUT4 <= (others => '0');
        OUT1 <= (others => '0');
        OUT6 <= (others => '0');
    ELSIF (SEL = "101") THEN
        OUT6 <= IN1;
        OUT2 <= (others => '0');
        OUT3 <= (others => '0');
        OUT4 <= (others => '0');
        OUT5 <= (others => '0');
        OUT1 <= (others => '0');
    ELSE
        OUT1 <= (others => '0');
        OUT2 <= (others => '0');
        OUT3 <= (others => '0');
        OUT4 <= (others => '0');
        OUT5 <= (others => '0');
        OUT6 <= (others => '0');
    END IF; -- if SEL

END PROCESS;

end behavior;
