library IEEE;
use IEEE.std_logic_1164.all;
use work.IEEE_1164_Gates_pkg.all;

architecture behavior of gates is

signal G0, G1, G2, G3, G4 : std_logic;

begin


U1: NAND2 port map (A, C, G0);
U2: NOR2 port map(C , (not D), G1);
U3: NOR2 port map (A, D, G2);
U4: NAND3 port map ( (not A), B, D, G3);

U5: XNOR4 port map (G0, G1, (not G2), (not G3), O);

end behavior;
