library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture behavior of counter is

begin

PROCESS(CLK, RST, Enable, SyncLoadConstant, AsyncLoadInput, Input)

    VARIABLE Output_tmp : std_logic_vector( (4-1) downto 0 );

    BEGIN    
        --init variable 
        Output_tmp := "0000";
        --
        IF (AsyncLoadInput = '1') THEN
            Output_tmp := Input;
            Output <= Output_tmp;
        END IF; --AsyncLoadInput 
        IF ( rising_edge(CLK) ) THEN
            IF (RST = '1') THEN
                Output_tmp := "0000";
                Output <= Output_tmp;
            ELSIF (SyncLoadConstant = '1') THEN
                Output_tmp := "1111";
                Output <= Output_tmp;
            ELSIF (Enable = '1') THEN
                Output_tmp := std_logic_vector( unsigned(Output) + 1 );
                Output <= Output_tmp;
            ELSE
                Output <= Output;        
            END IF;                  
        END IF; --rising_edge(CLK)

END PROCESS;

end behavior;
