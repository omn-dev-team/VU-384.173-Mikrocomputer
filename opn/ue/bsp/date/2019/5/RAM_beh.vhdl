library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;

architecture Behavioral of RAM is

    CONSTANT word_size : natural := 10;
    CONSTANT ram_size : natural := 2**8;

    CONSTANT high_imp : std_logic_vector( word_size-1 downto 0 ) := (OTHERS => 'Z');

    SUBTYPE word_t IS std_logic_vector( word_size-1 downto 0 );
    TYPE memory_t IS array(0 to ram_size-1) OF word_t;

    SIGNAL ram : memory_t := (OTHERS => (OTHERS => '0'));

begin

PROCESS(Clk)

    VARIABLE choice : std_logic_vector(1 downto 0);

BEGIN
    IF ( rising_edge(Clk) ) THEN
        output <= high_imp;
        choice := en_read & en_write;
    
        CASE choice IS
--nur en_write eingang auf addr1
            WHEN "01" =>
                ram( to_integer( unsigned( addr1 ) ) ) <= input;
--nur en_read output von addr2
            WHEN "10" =>
                output <= ram( to_integer( unsigned( addr2 ) ) );
--en_read und en_write read vor write
            WHEN "11" =>
                output <= ram( to_integer( unsigned( addr2 ) ) );                                
                ram( to_integer( unsigned( addr1 ) ) ) <= input;                   
            WHEN OTHERS => NULL;

        END CASE;

    END IF; --rising_edge

END PROCESS;

end Behavioral;
