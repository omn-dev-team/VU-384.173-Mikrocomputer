library IEEE;
use IEEE.std_logic_1164.all;
use work.fsm_pkg.all;

architecture behavior of fsm is

signal fsm: fsm_state;

begin

    PROCESS (CLK, RST, INPUT, fsm)
    
    BEGIN

        IF ( rising_edge(CLK) ) THEN
            IF ( RST = '1') THEN
                OUTPUT <= "00";
                fsm <= START;
                STATE <= START;
            ELSE 
                
                CASE fsm IS

                    WHEN START =>
                        IF ( INPUT = "11" ) THEN
                            OUTPUT <= "11";
                            fsm <= S2;
                            STATE <= S2;
                        ELSE
                            OUTPUT <= "00";
                        END IF;

                    WHEN S2 =>
                        IF ( INPUT = "01" ) THEN
                            OUTPUT <= "01";
                            fsm <= S1;
                            STATE <= S1;
                        ELSIF ( INPUT = "11" ) THEN
                            OUTPUT <= "11";
                            fsm <= S0;
                            STATE <= S0;
                        ELSE
                            OUTPUT <= "00";
                        END IF;

                    WHEN S1 =>
                        IF ( INPUT = "01" ) THEN
                            OUTPUT <= "10";
                            fsm <= S0;
                            STATE <= S0;
                        ELSE
                            OUTPUT <= "00";
                        END IF;

                    WHEN S0 =>
                        IF ( INPUT = "00" ) THEN
                            OUTPUT <= "01";
                            fsm <= S0;
                            STATE <= S0;
                        ELSIF ( INPUT = "11" ) THEN
                            OUTPUT <= "11";
                            fsm <= S2;
                            STATE <= S2;
                        ELSE
                            OUTPUT <= "00";
                        END IF;

                    WHEN others =>
                        NULL;


                END CASE;
            END IF; --RST ?
        END IF; --rising_edge
    
END PROCESS;
       


end behavior;
